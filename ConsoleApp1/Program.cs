﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp1
{
    class Program
    {
        static void Main(string[] args)
        {
            using (upds1Entities db=new upds1Entities())
            {
                //rutina para insertar un registro
                //persona objPersonaA = new persona();
                //objPersonaA.nombre = "Sergio";
                //objPersonaA.edad = 20;
                //objPersonaA.ciudad_id = 1;
                //db.persona.Add(objPersonaA);
                //db.SaveChanges();


                //rutina de actualización de un registro
                //persona objPersonaU = db.persona.Where(d => d.nombre == "luis").First();
                //objPersonaU.edad = 15;
                //db.Entry(objPersonaU).State = System.Data.Entity.EntityState.Modified;
                //db.SaveChanges();


                // rutina para eliminar un registro
                //persona objPersonaD = db.persona.Find(2);
                //db.persona.Remove(objPersonaD);
                //db.SaveChanges();


                //rutina para listar datos
                var lista = db.persona;
                foreach (var objPersona in lista)
                {
                    Console.WriteLine(objPersona.nombre);
                }

            }
            Console.Read();
        }

    }
}
